import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "App Motos";
  verLoja = "Ver loja";
  cards = [
  {
    titulo: "Kawasaki Versys 650",
    subtitulo: "Desempenho versátil",
    conteudo: "Versatilidade é a base da Versys 650. Quer você esteja navegando pelas ruas da cidade ou enfrentando quilômetros de rodovias abertas, a Versys 650 foi construída para o conforto.",
    foto: "https://content2.kawasaki.com/ContentStorage/KMB/Products/4954/1dbc4bf5-7e23-4c2f-b8a6-1984e920c852.png?w=767"
  },
  {
    titulo: "Suzuki B-King",
    subtitulo: "Dona de um design extravagante, é pilotando essa Big Naked que podemos perceber as suas reais qualidades",
    conteudo: "Feita com base na naked GSX-S 1000, a moto abusa das linhas robustas e da ausência de carenagens para entregar algo semelhante à SV 650, só que com mais desempenho.",
    foto: "https://motos-motor.com.br/m/wp-content/uploads/precos-tabela-suzuki-gsx-1300-b-king.jpg"
  },
  {
    titulo: "Yamaha MT-09",
    subtitulo: "Design animal",
    conteudo: "Além de empolgar pelo desempenho inebriante, a Yamaha MT-09 faz quem está no guidão se sentir poderoso pelo design e rugido do excelente motor.",
    foto: "https://cdn.motor1.com/images/mgl/NvbmX/s1/4x3/yamaha-mt-09-2020.webp"
  },
  {
    titulo: "Harley-Davidson Fat Boy",
    subtitulo: "A Harley-Davidson é uma marca centenária e ao longo de sua existência tornou-se objeto de desejo dos norte-americanos.",
    conteudo: "Um motor poderoso que funciona suavemente, com resposta imediata do acelerador e um ronco puro que alegra a alma.",
    foto: "https://cdn.autopapo.com.br/box/uploads/2020/06/02113313/harley-davidson-fat-boy-30-anos-9-732x488.jpg"
  }
];

  constructor() {}

}
